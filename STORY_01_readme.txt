2A_03:
FR1 - Files extracted from disk image created from WinUser8-A

2A_04:
FR1 - Files manually copied from WinUser8-A Windows desktop

3A_user_01:
ForInv1 - download files from IE on user's machine. 
Zipped folder & loose files of previous version(s).

3A_user_02:
ForInv1 - download files from IE on user's machine. 
Unzipped folder 1 - downloadAllFiles()

3A_user_03:
ForInv1 - download files from IE on user's machine. 
Unzipped folder 2 - downloadAllFiles() after restoring deleted items

3A_forensic_01:
ForInv1 - download files from Chrome on user's machine. 
Zipped folder & loose files of previous version(s).

3A_forensic_02:
ForInv1 - download files from Chrome on user's machine. 
Unzipped folder 1 - downloadAllFiles()

3B_user:
ForInv1 - download files from desktop's client app on user's machine.

3B_forensic:
ForInv1 - download files from desktop's client app on forensic's machine.

import csv
import os


css_files = dict()

all_stage_labels = set()


def extract_rows(column_names: list, directory: str):

    csv_file_names = filter(lambda f: f.endswith('.csv'), os.listdir(directory))

    for stage_file_name in csv_file_names:

        stage_file_path = directory + '/' + stage_file_name

        with open(stage_file_path, mode='r') as csv_file:

            stage_label = stage_file_name[:-4]
            all_stage_labels.add(stage_label)

            csv_reader = csv.DictReader(csv_file)

            for row in csv_reader:

                file_path = row.get('FileNames') if 'FileNames' in row else row.get('FileName')
                file_name = os.path.split(file_path)[-1]

                css_file = css_files[file_name] = css_files.get(file_name, dict())
                css_file_stage = css_file[stage_label] = css_file.get(stage_label, dict())

                for column_name in column_names:
                    css_file_stage[column_name] = row.get(column_name, '')


hashing_data_fields = ['MD5', 'SHA1']

extract_rows(hashing_data_fields, 'data/hashings')

meta_data_fields =\
    [
        'Author',
         'CreateDate',
         'Creation-date',
         'Creator',
         'CurrentUser',
         'Date',
         'DateTimeOriginal',
         'Directory',
         'FileAccessDate',
         'FileCreateDate',
         'FileModifyDate',
         'FileName',
         'FilePermissions',
         'FileSize',
         'FileType',
         'LastModifiedBy',
         'ModifyDate',
         'Words'
     ]

extract_rows(meta_data_fields, 'data/metadata')

if not os.path.exists('analysis'):
    os.mkdir('analysis')

all_data_fields = hashing_data_fields + meta_data_fields

columns = list(all_stage_labels)
columns = sorted(columns)

for css_file_path, css_file_dict in css_files.items():
    css_file_name = os.path.split(css_file_path)[-1]

    with open('.\\analysis\\' + css_file_name + "_analysis.csv", mode='w') as css_file:
        csv_writer = csv.writer(css_file, lineterminator='\n', )

        csv_writer.writerow(["field"] + columns)

        for row_label in all_data_fields:
            row = [row_label]
            for column_label in columns:
                if column_label in css_file_dict:
                    row.append(css_file_dict.get(column_label).get(row_label))
                else:
                    row.append('')

            csv_writer.writerow(row)
